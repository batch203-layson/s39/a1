const express = require("express");

const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

// Route for creating course
router.post("/", courseControllers.addCourse);


module.exports = router;
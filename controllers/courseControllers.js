const Course = require("../models/Course");
const User = require("../models/User");


// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

/* 
module.exports.addCourse = (req, res) => {

    let newCourse = new Course({
        courseName: req.body.name,
        courseDescription: req.body.description,
        coursePrice: req.body.price,
        courseSlots: req.body.slots
        
    })
    console.log(newCourse);

    newCourse.save()
    .then(course=>{
        console.log(course);
        res.send(true);
    })
    .catch(err=>{
        console.log(err);
        res.send(false);
    })
}
 */
module.exports.addCourse = (req, res) => {

    let newCourse = new Course({
        courseName: req.body.name,
        courseDescription: req.body.description,
        coursePrice: req.body.price,
        courseSlots: req.body.slots

    })
    console.log(newCourse);
    // console.log(User.findOne({ isAdmin: req.body.isAdmin }));

    

    User.find({ isAdmin: false }, (err, result) => {
        
        console.log(result.isAdmin);
        if ({ isAdmin: false } ==false)
        {
            res.send({ message: "You don't have access to this page!" });
        }
        else
        {

            newCourse.save()
                .then(course => {
                    console.log(course);
                    res.send(true);
                })
                .catch(err => {
                    console.log(err);
                    res.send(false);
                })
        }
        /* 
        if (err) {
            return console.log(`You don't have acces to this page!`);
        }
        else {
            return res.status(200).send({
                Users: result
            })
        }
         */
    })
    
  /* 
   return User.findOne({ isAdmin: req.body.isAdmin })
        .then(result => {
            // User does not exists
            if (result == null) {
                //return res.send(false)
                return res.send({ message: "No User Found!" });
            }
            // User exists
            else {
                // Syntax: compareSync(data, encrypted)
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

                // If the passwords match/result of the above code is true.
                if (isPasswordCorrect) {
                    return res.send({ accessToken: auth.createAccessToken(result) });
                }
                else {
                    return res.send({ message: "Incorrect Password!" });
                }
            }
        })

         */
    // console.log(User.find({ isAdmin: req.body.isAdmin }));

    /* 
    User.findOne({isAdmin:req.body.isAdmin}).then(result => {
        console.log(result.isAdmin);
    })

    */
}



/* 
s39 Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
4. Update your remote link and push to git with the commit message of Add activity code - S39.
5. Add the link in Boodle.
 */